import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, RouteConfigLoadEnd, Router} from '@angular/router';
import { StockDetailsService } from './stock-details.service'

@Component({
  selector: 'app-stock-details',
  templateUrl: './stock-details.component.html',
  styleUrls: ['./stock-details.component.css']
})
export class StockDetailsComponent implements OnInit {
  ticker;
  response;
  dataArray = [];
  labelArray = [];
  allStrategy:any;
  validStock = true;
  selected = [];
  constructor(route: ActivatedRoute, private stockDetailsService:StockDetailsService, private router: Router) {
     this.ticker = route.snapshot.params.stock.toString()

   }
  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          userCallback (value, index, values) {
            return '£' + value.toString();
          }
        }
      }]
    },
  };
  public barChartLabels = this.labelArray;
  public barChartType = 'line';
  public barChartLegend = false;
  public barChartData = [
    {data: this.dataArray}
  ];

  formatDateLabel(tickdate){
    let date = new Date(tickdate);
    let hours = date.getHours();
    let minutes = "0" + date.getMinutes();
    let seconds = "0" + date.getSeconds();
    let formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    return formattedTime
  }

  getStrategies(){
    this.stockDetailsService.getStrategyData(this.ticker).subscribe((res)=>{
      this.allStrategy = res
    }, error=>{
      console.log(error)
    })
  }

  onSelect( { selected } ){
    console.log('Select event', selected, this.selected)
    this.router.navigate(['/strategydetails/' + selected[0].id])
  }

  onActivate(event){
    console.log('Activate event', event)
  }

  ngOnInit() {
    this.stockDetailsService.getPriceData(this.ticker,50).subscribe((res)=>{
      this.response = res
      this.response.forEach(tick => {
        this.dataArray.push(tick.price)
        this.labelArray.push(this.formatDateLabel(tick.recordedAt))
      });
      this.dataArray.reverse()
      this.labelArray.reverse()
      this.getStrategies()
    }, error=>{
        console.log(error)
        this.validStock = false
    })
  }

}
