import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class StockDetailsService {

  constructor(private http:HttpClient) { }

  //declare methods of the service
  priceUrl:string = environment.rest_host + '/price'
  stratUrl:string = environment.rest_host + '/strategy'


  getPriceData(ticker, limit){
    return this.http.get(`${this.priceUrl}/${ticker}/${limit}`)
    .pipe(
      catchError(this.handleError<Object[]>('getPriceData', []))
    );
  }

  getStrategyData(ticker){
    return this.http.get(`${this.stratUrl}/${ticker}`)
      .pipe(
        catchError(this.handleError<Object[]>('getStrategyData', []))
      );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // Let the app keep running by returning an empty result.
      return throwError(
        'Error Receiving Prices for this stock'
      )
    };
  }
}
