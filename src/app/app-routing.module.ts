import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewStrategyComponent } from './new-strategy/new-strategy.component';
import { StockDetailsComponent } from './stock-details/stock-details.component';
import { TradeManagementComponent } from './trade-management/trade-management.component'
import { StockManagementComponent } from './stock-management/stock-management.component'
import { StrategyManagementComponent } from './strategy-management/strategy-management.component'
import { StrategyDetailsComponent} from './strategy-details/strategy-details.component';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  {path: 'newstrategy', component: NewStrategyComponent},
  {path: 'stockdetails/:stock', component: StockDetailsComponent},
  {path: 'strategydetails/:strategy', component: StrategyDetailsComponent},
  {path: 'trademanagement', component: TradeManagementComponent},
  {path: 'stockmanagement', component: StockManagementComponent},
  {path: 'strategymanagement', component: StrategyManagementComponent},
  {path: '', component: HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
