import { Component, OnInit } from '@angular/core';
import { StrategyManagementService } from "./strategy-management.service";
import {Router} from '@angular/router';

@Component({
  selector: 'app-strategy-management',
  templateUrl: './strategy-management.component.html',
  styleUrls: ['./strategy-management.component.css']
})
export class StrategyManagementComponent implements OnInit {

  rows:any
  selected = []

  constructor(private strategyManagementService:StrategyManagementService, private router: Router) {
    this.strategyManagementService.getStrategyData().subscribe((res)=>{
      this.rows = res
      this.selected = []
    })
  }

  columns: any[] = [
    { prop: 'id', name: 'ID'} ,
    { prop: 'stock.ticker', name: 'Ticker' },
    { prop: 'currentPosition', name: 'currentPosition'},
    { prop: 'size', name: 'Volume' },
    { prop: 'lastTradePrice', name: 'lastTradePrice' },
    { prop: 'exitProfitLoss', name: 'exitProfitLoss' },
    { prop: 'profit', name: 'profit' },
    { prop: 'stopped', name: 'stopped' },
  ];


  onSelect( { selected } ){
    console.log('Select event', selected, this.selected)
    this.router.navigate(['/strategydetails/' + selected[0].id])
  }

  onActivate(event){
    console.log('Activate event', event)
  }
  ngOnInit() {

  }

  updateFilter(event) {

    this.strategyManagementService.getStrategyData().subscribe((res)=>{
      this.rows = res
      this.selected = []
      const val = event.target.value.toLowerCase();

      // filter our data
      const temp = this.rows.filter(function(d) {
        return d.stock.ticker.toLowerCase().indexOf(val) !== -1 || !val;
      });

      // update the rows
      this.rows = temp;

    })

  }

}
