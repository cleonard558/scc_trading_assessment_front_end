import { TestBed } from '@angular/core/testing';

import { StrategyManagementService } from './strategy-management.service';

describe('StrategyManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StrategyManagementService = TestBed.get(StrategyManagementService);
    expect(service).toBeTruthy();
  });
});
