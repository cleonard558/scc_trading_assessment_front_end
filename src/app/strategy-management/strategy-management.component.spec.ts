import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StrategyManagementComponent } from './strategy-management.component';

describe('StrategyManagementComponent', () => {
  let component: StrategyManagementComponent;
  let fixture: ComponentFixture<StrategyManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StrategyManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StrategyManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
