import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewBollingerComponent } from './new-bollinger.component';

describe('NewBollingerComponent', () => {
  let component: NewBollingerComponent;
  let fixture: ComponentFixture<NewBollingerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewBollingerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewBollingerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
