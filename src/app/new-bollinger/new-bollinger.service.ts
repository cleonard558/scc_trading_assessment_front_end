import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NewBollingerService {

  constructor(private http:HttpClient) { }

  postUrl = environment.rest_host +'/strategy'
  addNewBollingerStrategy(strategy){
    return this.http.post(this.postUrl, strategy)
      .pipe(
        catchError(this.handleError('postNewBollingerStrategy', []))
      );
  }


  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return throwError(
        'Error Posting New Simple Strategy'
      )
    };
  }
}
