import { Component, OnInit } from '@angular/core';
import {StockManagementService} from '../stock-management/stock-management.service';
import {NewBollingerService} from './new-bollinger.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-new-bollinger',
  templateUrl: './new-bollinger.component.html',
  styleUrls: ['./new-bollinger.component.css']
})
export class NewBollingerComponent implements OnInit {
  allStocks:any;
  inputStock = 1;
  inputVolume = 0;
  inputProfitLoss = 0;
  inputDuration = 0;
  msgString = '';
  msgClass = '';
  response:any;

  constructor(private stockManagementService:StockManagementService, private router: Router, private newSimpleService:NewBollingerService) {
    this.stockManagementService.getAllStock().subscribe((res)=>{
      this.allStocks = res;
    })
  }

  postToDB(){
    let findTickerObj = this.allStocks.find(o => o.id == this.inputStock);
    let strategy = {
      "id": -1,
      "size": this.inputVolume,
      "exitProfitLoss": this.inputProfitLoss,
      "stock": {
        "id": this.inputStock,
        "ticker": findTickerObj.ticker
      },
      "duration": this.inputDuration
    }
    this.newSimpleService
      .addNewBollingerStrategy(strategy)
      .subscribe(resp => {
        this.response = resp
        this.msgClass = 'alert alert-success alert-dismissible fade show text-center'
        this.msgString = `Success: New Bollinger Strategy with ID - ${this.response.id}, Created for Stock - ${this.response.stock.ticker}`
      //  Resetting Form Values
        this.inputStock = 1
        this.inputVolume = 0
        this.inputProfitLoss = 0
        this.inputDuration = 0
      }, error=>{
        console.log(error)
        this.msgClass = 'alert alert-danger alert-dismissible fade show text-center'
        this.msgString = error
      });
  }

  checkInput(){

    if(this.inputStock == null){
      this.msgString = 'A stock must be selected'
      this.msgClass = 'alert alert-danger alert-dismissible fade show text-center'
      return;
    }
    if(this.inputVolume == null){
      this.msgString = 'A volume must be input'
      this.msgClass = 'alert alert-danger alert-dismissible fade show text-center'
      return;
    }
    if(this.inputProfitLoss == null){
      this.msgString = 'A profit loss must be input'
      this.msgClass = 'alert alert-danger alert-dismissible fade show text-center'
      return;
    }
    if(this.inputDuration == null){
      this.msgString = 'A strategy duration must be input'
      this.msgClass = 'alert alert-danger alert-dismissible fade show text-center'
      return;
    }

    if(this.inputVolume <= 0){
      this.msgString = 'Volume must be greater than 0'
      this.msgClass = 'alert alert-danger alert-dismissible fade show text-center'
      return;
    }
    if(this.inputProfitLoss <= 0){
      this.msgString = 'Exit Profit Loss must be greater than 0'
      this.msgClass = 'alert alert-danger alert-dismissible fade show text-center'
      return;
    }
    if(this.inputDuration <= 0){
      this.msgString = 'Strategy Duration must be greater than 0'
      this.msgClass = 'alert alert-danger alert-dismissible fade show text-center'
      return;
    }

    this.postToDB()

  }

  submitBollingerStrategy(){
    this.checkInput()
  }

  ngOnInit(){

  }

}
