import { TestBed } from '@angular/core/testing';

import { NewBollingerService } from './new-bollinger.service';

describe('NewMovAvgService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NewBollingerService = TestBed.get(NewBollingerService);
    expect(service).toBeTruthy();
  });
});
