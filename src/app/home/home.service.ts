import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http:HttpClient) { }

   //declare methods of the service
   apiUrl:string = 'https://newsapi.org/v2/top-headlines?sources=bbc-news&' +
   'apiKey=f1a226dc4e8748f69588b96c1eb065b7'

  activeStratUrl = environment.rest_host + '/strategy/active'

   getNewsData(){
     return this.http.get(`${this.apiUrl}`)
     .pipe(
       catchError(this.handleError<Object[]>('getNewsData', []))
     );
   }

  getActiveStrategy(){
    return this.http.get(`${this.activeStratUrl}`)
      .pipe(
        catchError(this.handleError<Object[]>('getActiveStrategy', []))
      );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // Let the app keep running by returning an empty result.
      return throwError(
        'Error Receiving Home Component Data'
      )
    };
  }
}
