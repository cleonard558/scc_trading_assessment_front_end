import { Component, OnInit } from '@angular/core';
import { HomeService } from './home.service'
import {Observable, ObservedValueOf} from "rxjs";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private homeService:HomeService) { }
  articles;
  newsData:any;
  activeStrategy:any;
  ngOnInit() {
    this.homeService.getNewsData().subscribe((res)=>{
      this.newsData = res;
      this.newsData.articles = this.newsData.articles.slice(0,8)
    }, error=>{
        console.log(error)
    })

    this.homeService.getActiveStrategy().subscribe((data)=>{
      this.activeStrategy = data;
    }, error=>{
      console.log(error)
    })
  }

}
