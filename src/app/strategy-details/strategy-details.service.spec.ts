import { TestBed } from '@angular/core/testing';

import { StrategyDetailsService } from './strategy-details.service';

describe('StrategyDetailsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StrategyDetailsService = TestBed.get(StrategyDetailsService);
    expect(service).toBeTruthy();
  });
});
