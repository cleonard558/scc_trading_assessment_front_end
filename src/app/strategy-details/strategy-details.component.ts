import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, RouteConfigLoadEnd } from '@angular/router'
import { StrategyDetailsService} from '../strategy-details/strategy-details.service';

@Component({
  selector: 'app-strategy-details',
  templateUrl: './strategy-details.component.html',
  styleUrls: ['./strategy-details.component.css']
})
export class StrategyDetailsComponent implements OnInit {

  constructor(route: ActivatedRoute, private strategyDetailsService: StrategyDetailsService) {
    this.strategyId = route.snapshot.params.strategy
    this.strategyDetailsService.getStrategyData(this.strategyId).subscribe((res) => {
        this.response = res
      }
      , error => {
        console.log(error)
        this.validStrategy = false
      })


  }

  validStrategy = true;
  strategyId = -1;
  response:any;


  ngOnInit() {


  }
}
