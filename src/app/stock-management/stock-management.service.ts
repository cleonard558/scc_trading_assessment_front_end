import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StockManagementService {

  constructor(private http:HttpClient) { }

  //declare methods of the service
  priceUrl:string =  environment.rest_host +'/price/max'
  stockUrl:string =  environment.rest_host + '/stock'


  getStockData(){
    return this.http.get(`${this.priceUrl}`)
      .pipe(
        catchError(this.handleError<Object[]>('getStockPriceData', []))
      );
  }

  getAllStock(){
    return this.http.get(`${this.stockUrl}`)
      .pipe(
        catchError(this.handleError<Object[]>('getStockData', []))
      );
  }


  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return throwError(
        'Error Receiving All Trades'
      )
    };
  }
}
