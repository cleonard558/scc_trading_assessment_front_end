import { Component, OnInit } from '@angular/core';
import { StockManagementService } from './stock-management.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-stock-management',
  templateUrl: './stock-management.component.html',
  styleUrls: ['./stock-management.component.css']
})
export class StockManagementComponent implements OnInit {

  rows:any;
  selected = [];


  constructor(private stockManagementService:StockManagementService, private router: Router) {
    this.getData();
    setInterval(() => { this.getData(); }, 10000);

  }

  getData(){
    this.stockManagementService.getStockData().subscribe((res)=>{
      this.rows = res
    })
  }

  columns: any[] = [
    { prop: 'stock.id', name: 'ID'} ,
    { prop: 'stock.ticker', name: 'Ticker' },
    { prop: 'price', name: 'Price' },
    { prop: 'recordedAt', name: 'Last Updated' }
  ];

  onSelect( { selected } ) {
    console.log('Select event', selected, this.selected);
    this.router.navigate(['/stockdetails/' + selected[0].stock.ticker])
  }

  onActivate(event) {
    console.log('Activate event', event);
  }
  ngOnInit() {

  }

}
