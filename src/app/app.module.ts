import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts';
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable'


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewStrategyComponent } from './new-strategy/new-strategy.component';
import { StockDetailsComponent } from './stock-details/stock-details.component';
import { HomeComponent } from './home/home.component';
import { TradeManagementComponent } from './trade-management/trade-management.component';
import { StrategyManagementComponent } from './strategy-management/strategy-management.component';
import { StockManagementComponent } from './stock-management/stock-management.component';
import { NewBollingerComponent } from './new-bollinger/new-bollinger.component';
import {TooltipModule} from 'ng2-tooltip-directive';
import { StrategyDetailsComponent } from './strategy-details/strategy-details.component';

@NgModule({
  declarations: [
    AppComponent,
    NewStrategyComponent,
    StockDetailsComponent,
    HomeComponent,
    TradeManagementComponent,
    StrategyManagementComponent,
    StockManagementComponent,
    NewBollingerComponent,
    StrategyDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ChartsModule,
    HttpClientModule,
    NgxDatatableModule,
    FormsModule,
    TooltipModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
