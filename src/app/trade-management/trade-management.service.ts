import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class TradeManagementService {

  constructor(private http:HttpClient) { }

  //declare methods of the service
  apiUrl:string = environment.rest_host + '/trade'


  getTradeData(){
    return this.http.get(`${this.apiUrl}`)
    .pipe(
      catchError(this.handleError<Object[]>('getTradeData', []))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // Let the app keep running by returning an empty result.
      return throwError(
        'Error Receiving All Trades'
      )
    };
  }
}
