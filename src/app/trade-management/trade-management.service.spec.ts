import { TestBed } from '@angular/core/testing';

import { TradeManagementService } from './trade-management.service';

describe('TradeManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TradeManagementService = TestBed.get(TradeManagementService);
    expect(service).toBeTruthy();
  });
});
