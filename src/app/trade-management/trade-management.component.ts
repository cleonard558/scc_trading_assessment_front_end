import { Component, OnInit} from '@angular/core';
import { TradeManagementService } from './trade-management.service'

@Component({
  selector: 'app-trade-management',
  templateUrl: './trade-management.component.html',
  styleUrls: ['./trade-management.component.css']
})
export class TradeManagementComponent implements OnInit {
  rows:any
  selected = []

  constructor(private tradeManagementService:TradeManagementService) { 
    this.tradeManagementService.getTradeData().subscribe((res)=>{
      this.rows = res
      this.selected = []
    })
  }
  
  columns: any[] = [
    { prop: 'id', name: 'ID'} , 
    { prop: 'stockTicker', name: 'Ticker' }, 
    { prop: 'price', name: 'Price'},
    { prop: 'size', name: 'Volume' },
    { prop: 'state', name: 'State' },
    { prop: 'tradeType', name: 'Type' },
    { prop: 'lastStateChange', name: 'Last State Change' },
    { prop: 'accountedFor', name: 'Accounted For' }
  ];

  onSelect( { selected } ){
    console.log('Select event', selected, this.selected)
  }

  onActivate(event){
    console.log('Activate event', event)
  }
  ngOnInit() {

  }

  updateFilter(event) {

    this.tradeManagementService.getTradeData().subscribe((res)=>{
      this.rows = res
      this.selected = []
      const val = event.target.value.toLowerCase();

      // filter our data
      const temp = this.rows.filter(function(d) {
        return d.stockTicker.toLowerCase().indexOf(val) !== -1 || !val;
      });

      // update the rows
      this.rows = temp;
    })
    
  }

}
