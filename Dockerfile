FROM nginxinc/nginx-unprivileged:latest

EXPOSE 80

COPY dist/scc-frontend/ /usr/share/nginx/html/
